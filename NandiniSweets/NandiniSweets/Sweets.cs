﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NandiniSweets
{
    class Sweets
    {
        //Fields
        private int NoOfCustomers { set; get; }
        
        private int PriceOfSweets { set; get; }
       
        private bool ShopOpenOrClose { set; get; }

        private int NoOfSweets;

        public Sweets()
        {
            NoOfSweets = 50;
            
            ShopOpenOrClose = true;
        }

        public void Closeshop()
        {
            ShopOpenOrClose = false;
         
        }

        public bool GetShopStatus()
        {
            return ShopOpenOrClose;
        }

        public void BuySweet(int x)
        {
            Console.WriteLine("\nCustomer asking for " + x + " sweets.");
            if (NoOfSweets < x)
            {
                Console.WriteLine("Not enough sweets. Adding 50 more.");
                NoOfSweets += 50;
            }
            else
            {
                for (int i = 1; i <= x; i++)
                
                    NoOfSweets--;
                
            }

            Console.WriteLine("\nRemaining no of sweets is: " + NoOfSweets);
            
        }

        

        public void Openshop()
        {
            ShopOpenOrClose = true;
            
        }

        //Open Shop

        //Close Shop

        //Make Sweets
    }
}
