﻿using MVCDemo3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MVCDemo3.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult ShowStudents()
        {
            ViewBag.Message = "This page shows a simple list";

            //lets build a list. 

            //create a collection
            var ListOfStudents = new List<Students>();

            ListOfStudents = GetFiveStudents();


            //return View(db.NewsModel.ToList());
            return View(ListOfStudents);
        }

        private List<Students> GetFiveStudents()
        {
            //create a n empty list.
            var tempList = new List<Students>();

            //five super heroes. 
            var name = "";
            var roll =0;

            //five super heroes. 
            var tempStud1 = new Students();
            var tempStud2 = new Students();
            var tempStud3 = new Students();
            var tempStud4 = new Students();
            var tempStud5 = new Students();


            name = "Saloni";
            roll = 22;


            tempStud1.name = name;
            tempStud1.roll = roll;
            

            //add this hero to list. 
            tempList.Add(tempStud1);

            name = "Anu";
            roll = 24;


            tempStud2.name = name;
            tempStud2.roll = roll;


            //add this hero to list. 
            tempList.Add(tempStud2);

            name = "Panda";
            roll = 21;


            tempStud3.name = name;
            tempStud3.roll = roll;


            //add this hero to list. 
            tempList.Add(tempStud3);

            name = "Shibu";
            roll = 29;


            tempStud4.name = name;
            tempStud4.roll = roll;


            //add this hero to list. 
            tempList.Add(tempStud4);

            name = "Lol";
            roll = 20;


            tempStud5.name = name;
            tempStud5.roll = roll;


            //add this hero to list. 
            tempList.Add(tempStud5);


            return tempList;
        }

        public ActionResult Showlist()
        {
            ViewBag.Message = "This page shows a simple list";

            //lets build a list. 

            //create a collection
            var ListOfSuperHeroes = new List<SuperHero>();

            ListOfSuperHeroes = GetFiveSuperHeroes();


            //return View(db.NewsModel.ToList());
            return View(ListOfSuperHeroes);
        }

        public ActionResult Showlist2()
        {
            ViewBag.Message = "This page shows a student list";

            //lets build a list. 

            //create a collection
            var ListOfStudents = new List<Students>();

            ListOfStudents = GetFiveStudents();


            //return View(db.NewsModel.ToList());
            return View(ListOfStudents);
        }

        //this creates a list of super heros. 
        //we will display this in the view
        private static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();
            var tempHero2 = new SuperHero();
            var tempHero3 = new SuperHero();
            var tempHero4 = new SuperHero();
            var tempHero5 = new SuperHero();
            var tempHero6 = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 50;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero2.name = name;
            tempHero2.power = power;
            tempHero2.brand = brand;
            tempHero2.age = age;

            //add this hero to list. 
            tempList.Add(tempHero2);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero3.name = name;
            tempHero3.power = power;
            tempHero3.brand = brand;
            tempHero3.age = age;

            //add this hero to list. 
            tempList.Add(tempHero3);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero4.name = name;
            tempHero4.power = power;
            tempHero4.brand = brand;
            tempHero4.age = age;

            //add this hero to list. 
            tempList.Add(tempHero4);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 48;

            tempHero5.name = name;
            tempHero5.power = power;
            tempHero5.brand = brand;
            tempHero5.age = age;

            //add this hero to list. 
            tempList.Add(tempHero5);


            return tempList;
        }

    }


}