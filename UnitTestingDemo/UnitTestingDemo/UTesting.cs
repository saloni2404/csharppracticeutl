﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
    class UTesting
    {
        [TestClass]
        public class SampleTest
        {
            [TestMethod]
            public void TestSample1()
            {
                var actual = true;
                var expected = true;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSample2()
            {
                var actual = true;
                var expected = false;
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSample3()
            {
                var input1 = 85201.583475;
                var input2 = 5614.342;
                var actual = 90815.925475;
                var expected = Program.SumOfTwoNumbers(input1,input2);
                
                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSample4()
            {
                var input1 = 85201;
                var input2 = 5614;
                var actual = Program.SumOfTwoNumbers(input1, input2);
                var expected = 90815;

                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSample5()
            {
                var input1 = 45321;
                var input2 = 5698;
                var actual = Program.SumOfTwoNumbers(input1, input2);
                var expected = 51019;

                Assert.AreEqual(expected, actual);
            }
            [TestMethod]
            public void TestSample6()

            {
                var tempHero5 = new SuperHero();
                string name = "Batman";
                string power = "Money";
                string brand = "DC";
                int age = 30;

                tempHero5.name = name;
                tempHero5.power = power;
                tempHero5.brand = brand;
                tempHero5.age = age;

                //assign our expected value.
                var expectedhero = tempHero5;

                //get our actual value from the unit we are testing.
                //we are testing the unit ReturnSpecificHeroBasedOnAge
                //create a collection
                var ListOfSuperHeroes = new List<SuperHero>();
                ListOfSuperHeroes = Program.GetFiveSuperHeroes();
                int ageofhero = 30;
                SuperHero SpecificAgeSuperHero = Program.findByAge(ageofhero,ListOfSuperHeroes);
                var actualhero = SpecificAgeSuperHero;

                //okay set expected as true bool value.
                bool expected = true;
                //in the beginning set actual value to false.
                //if our test passes we will change it to true.
                bool actual = false;
                if (expectedhero.age == actualhero.age ||
                    expectedhero.brand == actualhero.brand ||
                    expectedhero.name == actualhero.name ||
                    expectedhero.power == actualhero.power)
                {
                    //all four values matching means, our unit is working correctly.
                    //set actual bool to true.
                    actual = true;
                }

                Assert.AreEqual(expected, actual);

            }

            [TestMethod]
            public void TestSample7()

            {
                var tempHero5 = new SuperHero();
                string name = "Batman";
                string power = "Money";
                string brand = "Marvel";
                int age = 30;

                tempHero5.name = name;
                tempHero5.power = power;
                tempHero5.brand = brand;
                tempHero5.age = age;

                //assign our expected value.
                var expectedhero = tempHero5;

                //get our actual value from the unit we are testing.
                //we are testing the unit ReturnSpecificHeroBasedOnAge
                //create a collection
                var ListOfSuperHeroes = new List<SuperHero>();
                ListOfSuperHeroes = Program.GetFiveSuperHeroes();
                int ageofhero = 30;
                SuperHero SpecificAgeSuperHero = Program.findByAge(ageofhero,ListOfSuperHeroes);
                var actualhero = SpecificAgeSuperHero;

                //okay set expected as true bool value.
                bool expected = true;
                //in the beginning set actual value to false.
                //if our test passes we will change it to true.
                bool actual = false;
                if (expectedhero.age == actualhero.age &&
                    expectedhero.brand == actualhero.brand &&
                    expectedhero.name == actualhero.name &&
                    expectedhero.power == actualhero.power)
                {
                    //all four values matching means, our unit is working correctly.
                    //set actual bool to true.
                    actual = true;
                }

                Assert.AreEqual(expected, actual);

            }
        }
    }
}
