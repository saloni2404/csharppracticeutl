﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTestingDemo
{
      
    class Program
    {
        public void TestcheckForChangeSolicitor()
        {
            var actual = true;
            var expected = true;
            //Assert.AreEqual(expected, actual);
        }
        static void Main(string[] args)
        {
            /*
            double num1=10;
            double num2 = 20;
            double resultOfAddition = 0.0;
            resultOfAddition = SumOfTwoNumbers(num1,num2);
            */

            //create a collection
            var ListOfSuperHeroes = new List<SuperHero>();
            ListOfSuperHeroes = GetFiveSuperHeroes();
            SuperHero temp = findByAge(30, ListOfSuperHeroes);

           

            Console.ReadLine();
        }

        public static double SumOfTwoNumbers(double num1, double num2)
        {
            return num1 + num2;
        }

        public static SuperHero findByAge(int age, List<SuperHero> finalList)
        {
            SuperHero temppHero = new SuperHero();
            temppHero = finalList.Select(x => x).Where(x => x.age == age).FirstOrDefault();
            return temppHero;
        }

        public static List<SuperHero> GetFiveSuperHeroes()
        {
            //create a n empty list.
            var tempList = new List<SuperHero>();

            //five super heroes. 
            var name = "";
            var power = "";
            var brand = "";
            var age = 0;

            //five super heroes. 
            var tempHero = new SuperHero();

            name = "Batman";
            power = "Money";
            brand = "DC";
            age = 30;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);

            tempHero = new SuperHero();

            name = "Superman";
            power = "Superhuman Strength";
            brand = "DC";
            age = 42;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);

            tempHero = new SuperHero();

            name = "Wonderwoman";
            power = "Strength";
            brand = "DC";
            age = 28;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);

            tempHero = new SuperHero();

            name = "Captain America";
            power = "Shield";
            brand = "Marvel";
            age = 39;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);

            tempHero = new SuperHero();

            name = "Ironman";
            power = "Armor";
            brand = "Marvel";
            age = 40;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);

            tempHero = new SuperHero();

            name = "Hulk";
            power = "Strength";
            brand = "Marvel";
            age = 38;

            tempHero.name = name;
            tempHero.power = power;
            tempHero.brand = brand;
            tempHero.age = age;

            //add this hero to list. 
            tempList.Add(tempHero);


            return tempList;
        }

        public static Boolean whatIsIt(SuperHero s1 , SuperHero s2)
        {
            if ((s1.age == s2.age) && (s1.brand == s2.brand) && (s1.name == s2.name) && (s1.power == s2.power))
                return true;
            else
                return false;
            
        }
    }
}
