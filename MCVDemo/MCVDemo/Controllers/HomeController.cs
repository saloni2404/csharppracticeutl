﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MCVDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message1 = "Hi there I am Saloni.";
            ViewBag.Message2 = "I am weird.";
            ViewBag.Message3 = "Ok bye";
            return View();
        }

        public ActionResult SuperHeroes()
        {
            ViewBag.Message1 = "Batman";
            ViewBag.Message2 = "Superman";
            ViewBag.Message3 = "Wonderwoman";
            ViewBag.Message4 = "Flash";
            ViewBag.Message5 = "Spiderman";

            return View();
        }
    }
}